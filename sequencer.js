var path = require('path');
var spawn = require("child_process").spawn;
var net = require('net');
var fs = require('fs');
var events = require('events');
var jsonpack = require('jsonpack/main');
var redis = require('redis');
var getVideoDimensions = require('get-video-dimensions');
var { getVideoDurationInSeconds } = require('get-video-duration');

module.exports.Sequencer = class Sequencer {

    constructor(db, port, dumpSize) {

        /* Config */
        this.db = db;
        this.port = port;
        this.detsNb = 0;
        this.files = [];
        this.cursor = 0;
        this.used = false;
        this.assoc = undefined;
        this.name = undefined;
        this.url = undefined;
        this.duration = undefined;
        this.dimensions = undefined;
        this.dumpSize = dumpSize;
        this.redisClient = redis.createClient();
        this.player = new events.EventEmitter();
        this.redisClient.del('visiolab-vision-detections');
        this.supnetClient = new net.Socket();
        this.supnetClient.connect(this.port, "127.0.0.1");
        this.supnetClient.on('data', (data) => {
            var dataJSON = JSON.parse(data);

            dataJSON.secDuration = this.duration;
            dataJSON.progressSec = this.cursor * 1000;
            dataJSON.url = this.url;
            dataJSON.assoc = this.assoc;
            dataJSON.src = 'http://127.0.0.1:8002/sequences/' + this.name + '/' + this.files[this.cursor];
            dataJSON.dets = this.rectifyYoloCoords(dataJSON.dets, this.dimensions.width, this.dimensions.height);
            console.log('> ' + this.name + ' sequence at ' + dataJSON.progressSec + '/' + dataJSON.secDuration + ' in seconds');
            this.detsNb += 1;
            this.cursor += 1;
            this.save(dataJSON);
            this.player.emit('result', dataJSON);
            this.player.emit('next');
        });
        this.player.setMaxListeners(0);
        if (!fs.existsSync('public'))
            fs.mkdir('public', { recursive: true }, (err) => { if (err) throw err; });
        if (!fs.existsSync('public/sequences'))
            fs.mkdir('public/sequences', { recursive: true }, (err) => { if (err) throw err; });

    }

    rectifyYoloCoords(dets_, width, height) {
        var dets = [];

        for (var key in dets_) {
            /* Pourc to pixels and rectify*/
            var x = dets_[key].xmin * width;
            var y = dets_[key].ymin * height;
            var w = (dets_[key].xmax - dets_[key].xmin) * width;
            var h = (dets_[key].ymax - dets_[key].ymin) * height;
            dets.push({
                "class": dets_[key].class,
                "color": dets_[key].color,
                "x": x,
                "y": y,
                "w": w,
                "h": h
            });
        }
        return (dets);
    }

    save(data) {

        let packed;

        packed = jsonpack.pack(data);
        if (data.res == 1)
            this.redisClient.rpush('visiolab-vision-detections', JSON.stringify(packed));
        if ((this.detsNb != 0 && this.detsNb % this.dumpSize == 0) || this.dumpSize == 1)
            this.dumpCache();

    }

    dumpCache() {

        let saveJSON = {};
        let arrayJSON = [];

        this.redisClient.lrange('visiolab-vision-detections', 0, this.dumpSize - 1, (err, replies) => {
            if (err) throw err;

            for (let i = 0; i < replies.length; i++) {
                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                arrayJSON.push(saveJSON);
            }
            if (arrayJSON.length > 0) {
                this.db.collection('detections').insertMany(arrayJSON, (err, dbResult) => {
                    if (err) throw err;

                    this.redisClient.del('visiolab-vision-detections');
                });
            }
        });

    }

    sortNumber(a, b) {
        return (a - b);
    }

    sequences(name, url, assoc) {
        var sequencesDir = path.join('public', 'sequences', name);

        getVideoDurationInSeconds(url).then((duration) => {
            this.duration = Math.round(duration) * 1000;
            getVideoDimensions(url).then((dimensions) => {
                this.dimensions = dimensions;
                this.name = name;
                this.url = url;
                this.assoc = assoc;
                this.used = true;
                this.cursor = 0;
                if (!fs.existsSync(path.join(sequencesDir)))
                    fs.mkdir(sequencesDir, { recursive: true }, (err) => { if (err) throw err; });

                var ffprobe = spawn('ffprobe', ['-v', 'error', '-select_streams', 'v:0', '-show_entries', 'stream=codec_name', '-of', 'default=noprint_wrappers=1:nokey=1', url]);

                ffprobe.stdout.once('data', (data) => {

                    var codec = data.toString().substr(0, data.toString().length - 1) + '_cuvid';
                    var ffmpeg = spawn('ffmpeg', ['-hwaccel', 'cuvid', '-c:v', codec, '-i', url, '-vf', 'hwdownload,format=nv12,fps=1', path.join(sequencesDir, '%d.jpg')]);

                    console.log('[SEQUENCER] ' + this.name + ' launch');
                    ffmpeg.once('close', () => {
                        new Promise((resolve, reject) => {
                            console.log('> ' + this.name + ' sequences finished');
                            fs.readdir(sequencesDir, (error, files) => {
                                var tmp = [];
                                var file;

                                for (var i = 0; i < files.length; i++) {
                                    file = files[i];
                                    tmp.push(parseInt(file.substr(0, file.length - 4)));
                                }
                                tmp.sort(this.sortNumber);
                                for (var i = 0; i < tmp.length; i++) {
                                    this.files.push(tmp[i].toString() + '.jpg');
                                }
                                resolve();
                            });
                        }).then(() => {
                            this.player.on('next', () => {
                                if (this.cursor < this.files.length)
                                    this.supnetClient.write('{"filename":"' + path.join('..', sequencesDir, this.files[this.cursor]) + '"}');
                                else {
                                    this.used = false;
                                    this.player.removeAllListeners();
                                    this.files = [];
                                    this.dumpCache();
                                    /* Update detected at true */
                                    this.db.collection("active_videos").updateOne({ 'jsonRes._id': this.assoc }, {$set: {"jsonRes.detected": 1}}, (err) => {
                                        if (err) throw err;

                                        console.log('[SEQUENCER] ' + this.name + ' end');
                                    });
                                }
                            });
                            this.player.emit('next');
                        });
                    });
                });
            });
        });
    }

};

/*const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/vision';

(async function() {
    try {

        const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true } );
        const db = await client.db();

        let sequencer = new Sequencer(db,  1818, 70);
        sequencer.sequences('gent10-v2', 'http://127.0.0.1:8000/videos/gent10.mp4');
        client.close();

    } catch (e) {

    }
})();*/
