/* Requires */

const jwt = require('jsonwebtoken');

/* Class */

module.exports = class Collections {

	constructor(app, db, sequencer) {
		this.app = app;
		this.db = db;
		this.sequencer = sequencer;
	}

	create(method, uri, managePrivileges, resource) {
		if (method.toUpperCase() == 'GET') {
			this.app.get(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, this.sequencer);
	            });
	        });
		} else if (method.toUpperCase() == 'POST') {
			this.app.post(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, this.sequencer);
	            });
	        });
		} else if (method.toUpperCase() == 'PUT') {
			this.app.put(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, this.sequencer);
	            });
	        });
		} else if (method.toUpperCase() == 'DELETE') {
			this.app.delete(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, this.sequencer);
	            });
	        });
		} else {
			console.warn("[COLLECTIONS] Unknown method");
		}
	}

};