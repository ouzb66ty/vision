const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/vision';

/* Database ------------------------------------------------------- */

(async function() {
    try {

        /* Requires & Variables ------------------------------------------ */

        const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true } );
        const express = require('express');
        const cors = require('cors');
        const api = express();
        const bodyParser = require('body-parser');
        const resources = require('./resources');
        const db = await client.db();
        const wsServer = require('http').Server(api);
        const collectionsModule = require('./models/collections');
        const dumpSize = 300;
        const initModule = require('./init');
        const init = new initModule.Init({

            'dumpSize': dumpSize,
            'wsServer': wsServer,
            'db': db

        });
        const collections = new collectionsModule(api, db, init.sequencer);

        /* Initialize WS server & video sequencer ------------------------- */

        init.startAll();

        /* Express configuration ------------------------------------------ */

        api.use(express.static('public'));
        api.use(cors());
        api.use(bodyParser.json());
        api.use(bodyParser.urlencoded({ extended: true }));

        /* Get Bearer token for auth -------------------------------------- */

        function managePrivileges(req, res, next) {

            const bearerHeader = req.headers["authorization"];

            if (typeof bearerHeader != 'undefined') {
                const bearer = bearerHeader.split(" ");
                const bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.statusCode = 401;
                res.json({
                    'error': 'Need correct API token in Bearer token'
                });
            }
        }

        /* Collections */

        collections.create("DELETE", '/vision/v1/areas/:id', managePrivileges, resources.API.deleteArea);
        collections.create("GET", '/vision/v1/areas/:id', managePrivileges, resources.API.getVideoAreas);
        collections.create("GET", '/vision/v1/active_videos/:n', managePrivileges, resources.API.getNActiveVideos);
        collections.create("GET", '/vision/v1/active_videos', managePrivileges, resources.API.getActiveVideos);
        collections.create("POST", '/vision/v1/active_videos', managePrivileges, resources.API.postActiveVideos);
        collections.create("GET", '/vision/v1/detections', managePrivileges, resources.API.getDetections);
        collections.create("GET", '/vision/v1/detections/videos/:id/progressSec/:progressSec', managePrivileges, resources.API.getOneDetection);
        collections.create("GET", '/vision/v1/detections/videos/:id/search', managePrivileges, resources.API.postSearchVideosDetections);
        collections.create("GET", '/vision/v1/detections/videos/:id', managePrivileges, resources.API.getVideoDetections);
        collections.create("POST", '/vision/v1/areas', managePrivileges, resources.API.postAreas);
        collections.create("POST", '/vision/v1/detections', managePrivileges, resources.API.postDetections);
        collections.create("POST", '/vision/v1/export/:id', managePrivileges, resources.API.postExport);
        collections.create("DELETE", '/vision/v1/assoc/:id', managePrivileges, resources.API.deleteAssoc);

        /* Port Binding --------------------------------------------------- */

        api.listen(8003);
        wsServer.listen(8002);

    } catch(e) {

        /* Database Error Connection ------------------------------------------ */

        console.error(e)

    }

})();
