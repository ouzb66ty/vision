/*
 * All requires
 */

const sequencerModule = require('./sequencer');
const redis = require('redis');

/*
 * Initialize all needed process and Sequencer,
 * For the detections orchestrations
 */

module.exports.Init = class Init {

    constructor(config) {

        this.wsServer = config.wsServer;
        this.db = config.db;
        this.dumpSize = config.dumpSize;
        this.io = require('socket.io')(this.wsServer);
        this.sequencer = new sequencerModule.Sequencer(this.db, 1717, this.dumpSize);
        this.redis = require('redis');
        this.client = redis.createClient();

    }

    startWsServer() {

        let sequencer = this.sequencer;

        this.io.on('connection', function(socket) {
            sequencer.player.on('result', (data) => {
                socket.emit('dets', data);
            });
        });

    }

    startAll() {

        this.startWsServer();

    }

};
