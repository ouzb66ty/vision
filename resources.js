const jsonpack = require('jsonpack/main');
const PDFDocument = require('pdfkit');
const fs = require('fs');

module.exports.API = class API {

    static deleteAssoc(db, req, res, sequencer) {

        const ObjectID = require('mongodb').ObjectID;

        var deleteFolderRecursive = function(path) {
            if( fs.existsSync(path) ) {
                fs.readdirSync(path).forEach(function(file,index){
                    var curPath = path + "/" + file;
                    if(fs.lstatSync(curPath).isDirectory()) {
                        deleteFolderRecursive(curPath);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };

        db.collection('areas').deleteMany({'assoc': req.params.id});
        db.collection('active_videos').findOne({'jsonRes._id': req.params.id}, (err, active_video) => {
            if (err) throw err;

            if (active_video) {
                deleteFolderRecursive('public/sequences/' + active_video.jsonRes.name);
                db.collection('active_videos').deleteMany({'jsonRes._id': req.params.id});
                db.collection('detections').deleteMany({'assoc': req.params.id});
            }
        });
        res.json({'success': 'All associate datas from videos deleted'});
    }

    /**
     * @api {get} /vision/v1/active_videos Get all active videos
     * @apiName getActiveVideos
     * @apiGroup active_videos
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} videos All active videos
     *
     * @apiParam {Number} n Active videos number to give.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "active_videos": [{
     *               "_id": "5c9295e5f0fa6942e3297c1b",
     *               "pathname": "/public/videos/brigade.webm",
     *               "namespace": "brigade"
     *           }]
     *      }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getActiveVideos(db, req, res, sequencer) {

        db.collection('active_videos').find({}).toArray((err, active_videos) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {
                res.json({ active_videos });
            }
        });

    }

    /**
     * @api {get} /vision/v1/active_videos Get the N last active videos
     * @apiName getNActiveVideos
     * @apiGroup active_videos
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} videos  N last active videos
     *
     * @apiParam {Number} n Active videos number to give.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "active_videos": [{
     *               "_id": "5c9295e5f0fa6942e3297c1b",
     *               "pathname": "/public/videos/brigade.webm",
     *               "namespace": "brigade"
     *           }]
     *      }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getNActiveVideos(db, req, res, sequencer) {

        db.collection('active_videos').count().then((count) => {

            db.collection('active_videos').find({}).skip(count - req.params.n).toArray((err, active_videos) => {

                if (err) {
                    res.statusCode = 500;
                    res.json({ 'error' : 'Internal Server Error' });
                    throw err;
                } else {
                    res.json({ active_videos });
                }

            });

        });

    }

    /**
     * @api {post} /vision/v1/active_videos Add new video in detection mode
     * @apiName postActiveVideos
     * @apiGroup active_videos
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new active video (detection on)
     *
     * @apiParam {Number} id Video Users unique ID.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Successfully add new active video (detection on)'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 502 Bad Gateway ou Proxy Error
     *     {
     *          'error': "Can't obtain Video ID from Orizon API"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Already active'
     *     }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 449 Internal Server Error
     *     {
     *          'error': 'Please wait loading..'
     *     }
     */

    static postActiveVideos(db, req, res, sequencer) {

        const ObjectID = require('mongodb').ObjectID;
        const request = require('request');
        let msg = {};

        if (sequencer.used == false) {
            request({

                url: 'http://127.0.0.1:8001/orizon/v1/videos/id/' + req.body.id,
                method: 'GET',
                headers: {
'Authorization': 'Bearer ' + req.token,
'Content-Type': 'application/json'
                }

            }, (err, resp, body) => {

                if (err) {
                    res.statusCode = 500;
                    res.json({ 'error' : 'Internal Server Error' });
                    throw err;

                } else {

                    const jsonRes = JSON.parse(body);

                    jsonRes.detected = false;
                    if (resp.statusCode === 404 ) {

                        res.statusCode = 502;
                        res.json( { 'error' : "Can't obtain Video ID from Orizon API" } );

                    } else {

                        /* Video detection already ON verification */
                        db.collection('active_videos').findOne({ 'jsonRes._id' : req.body.id },
                            (err, active_video) => {

                                /* Insert new video detection ON */
                                db.collection('active_videos').insertOne({jsonRes}, (err, dbRes) => {

                                    if (err) {

                                        res.statusCode = 500;
                                        res.json({ 'error' : 'Internal Server Error' });
                                        throw err;

                                    } else {

                                        /* Sequence video & detect */
                                        sequencer.sequences(jsonRes.name, encodeURI("http://127.0.0.1:8000/videos/" + jsonRes.namespace + "/" + jsonRes.name), req.body.id);
                                        res.json( { 'success' : "Successfully add new active video (detection on)" } );

                                    }

                                });

                            });

                    }

                }

            });
        } else {

            res.statusCode = 449;
            res.json({ 'error' : 'Sequencer active' });

        }

    }

    /**
     * @api {get} /vision/v1/areas/{id} Delete one area
     * @apiName deleteArea
     * @apiGroup areas
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} areas {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "areas": []
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static deleteArea(db, req, res, user) {

        const ObjectID = require('mongodb').ObjectID;

        db.collection('areas').deleteOne(
            {
                'name': req.body.name,
                'coords.0': req.body.x,
                'coords.1': req.body.y,
                'coords.2': req.body.w,
                'coords.3': req.body.h
            },
            (err, dbResult) => {
                if (err) throw err;

                res.json(dbResult);
            });

    }

    /**
     * @api {get} /vision/v1/areas/{id} Get all areas
     * @apiName getVideoAreas
     * @apiGroup areas
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} areas {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "areas": []
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getVideoAreas(db, req, res, sequencer) {

        db.collection('areas').find({'assoc': req.params.id}).toArray(

            (err, areas) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({areas});

                }

            });

    }

    /**
     * @api {get} /vision/v1/detections Get all detections from active_cameras sources
     * @apiName getDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} detections {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "detections": [
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551af",
     *                   "filename": "../public/sequences/Plage/169c5026fef.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *               },
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551b0",
     *                   "filename": "../public/sequences/Plage/169c5026a23.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *                }
     *          ]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getDetections(db, req, res, sequencer) {

        const redis = require('redis');
        const client = redis.createClient();
        let saveJSON;
        let arrayJSON = [];

        db.collection('detections').find({}).toArray((err, detections) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {
                client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {

                    if (err) throw err;

                    for (let i = 0; i < replies.length; i++) {
                        saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                        arrayJSON.push(saveJSON);
                    }
                    detections = detections.concat(arrayJSON);
                    client.quit();
                    res.json({detections});
                });
            }
        });

    }

    /**
     * @api {get} /vision/v1/detections/videos/{id} Get all detections from active videos ID
     * @apiName getVideoDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} detections {}
     *
     * @apiParam {id} id Associate id video.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "detections": [
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551af",
     *                   "filename": "../public/sequences/Plage/169c5026fef.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *               },
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551b0",
     *                   "filename": "../public/sequences/Plage/169c5026a23.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *                }
     *          ]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getVideoDetections(db, req, res, sequencer) {

        const ObjectID = require('mongodb').ObjectID;
        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                        if (replies) {
                            for (let i = 0; i < replies.length; i++) {
                                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                if (saveJSON.assoc == req.params.id)
                                    arrayJSON.push(saveJSON);
                            }
                        }
                        detections = detections.concat(arrayJSON);
                        client.quit();
                        res.json({detections});
                    });

                }

            });
    }

    /**
    */

    static getOneDetection(db, req, res, sequencer) {

        const ObjectID = require('mongodb').ObjectID;
        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();
        var detection = null;

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                        if (replies) {
                            for (let i = 0; i < replies.length; i++) {
                                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                if (saveJSON.assoc == req.params.id)
                                    arrayJSON.push(saveJSON);
                            }
                        }
                        detections = detections.concat(arrayJSON);
                        client.quit();
                        for (var i = 0; i < detections.length; i++) {
                            if (detections[i].progressSec == parseInt(req.params.progressSec)) {
                                detection = detections[i];
                            }
                        }
                        res.json({detection});
                    });

                }

            });
    }

    /**
    */

    static postExport(db, req, res, sequencer) {

        const ObjectID = require('mongodb').ObjectID;
        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();
        var detection = null;
        var Pdfkit = require('pdfkit');
        var fs = require('fs');

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    new Promise((resolve, reject) => {

                        /* Associate Redis Dump to MongoDB datas */
                        client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                            if (replies) {
                                for (let i = 0; i < replies.length; i++) {
                                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                    if (saveJSON.assoc == req.params.id)
                                        arrayJSON.push(saveJSON);
                                }
                            }
                            detections = detections.concat(arrayJSON);
                            client.quit();
                            for (var i = 0; i < detections.length; i++) {
                                if (detections[i].progressSec == parseInt(req.body.progressSec)) {
                                    detection = detections[i];
                                }
                            }

                            resolve(detection);
                        });

                    }).then((detection) => {

                        /* Generate PDF */
                        var doc = new Pdfkit;
                        var ext = req.body.statsDataUrl.split(';')[0].match(/jpeg|png|gif/)[0];
                        var data = req.body.statsDataUrl.replace(/^data:image\/\w+;base64,/, "");
                        var buf = new Buffer.from(data, 'base64');

                        if (!fs.existsSync('public/rapports'))
                            fs.mkdir('public/rapports', { recursive: true }, (err) => { if (err) throw err; });
                        doc.pipe(fs.createWriteStream('public/rapports/rapport.pdf'));
                        doc.image(detection.filename.substr(3, detection.filename.length), 10, 127, { width: 592 });
                        fs.writeFile('/tmp/tmp-export-visiolab.' + ext, buf, (err) => {
                            if (err) throw err;

                            doc.image('/tmp/tmp-export-visiolab.png', 10, 472, { width: 592 });
                            db.collection('active_videos').findOne({'jsonRes._id': req.params.id}, (err, active_video) => {
                                doc.fontSize(10).text('Domaine: ' + active_video.jsonRes.namespace, 25, 35);
                                doc.fontSize(10).text('Nom du fichier: ' + active_video.jsonRes.name, 25, 47);
                                doc.fontSize(10).text('Type: ' + active_video.jsonRes.mimetype, 25, 59);
                                doc.fontSize(10).text('Sequence: ' + detection.progressSec / 1000 + '/' + detection.secDuration / 1000 + 'sec', 25, 71);
                                doc.fontSize(10).text('Auteur de l\'analyse: ' + active_video.jsonRes.username, 25, 83);
                                doc.fontSize(10).text('Vidéo téléchargé ' + active_video.jsonRes.date, 25, 95);
                                doc.fontSize(8).text('Visiolab - Rapport de détection', 5, 5);
                                doc.end();
                                res.json({ 'url': 'http://127.0.0.1:8003/rapports/rapport.pdf' });
                            });
                        });

                    });

                }

            });

    }

    /**
     * @api {post} /vision/v1/areas Apply new areas filters
     * @apiName postAreas
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new areas filters.
     *
     * @apiParam {Object} areas Contains name and coords of all areas.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Areas successfully added'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postAreas(db, req, res, sequencer) {

        for (let i = 0; i < req.body.length; i++) {
            if (!req.body[i].name || !req.body[i].coords || req.body[i].coords.length != 4) {
                res.statusCode = 400;
                res.json({ 'error' : 'Bad Parameters' });
                return ;
            }
        }

        db.collection('areas').insertOne(req.body, (err, dbResult) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
            } else {
                res.json({
                    'success' : 'Areas successfully added'
                });
            }
        }, { ordered: false },);

    }

    /**
     * @api {post} /vision/v1/detections Dump Redis detections in MongoDB
     * @apiName postDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new active camera (detection on)
     *
     * @apiParam {Number} dumpSize Redis dump size to stock in MongoDB.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Dump successfully'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postDetections(db, req, res, sequencer) {

        const redis = require('redis');
        const client = redis.createClient();
        const jsonpack = require('jsonpack');
        const dumpSize = req.body.dumpSize;
        let saveJSON = {};
        let arrayJSON = [];

        client.lrange("dets", 0, sequencer.dumpSize, (err, replies) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {
                for (let i = 0; i < replies.length; i++) {
                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                    arrayJSON.push(saveJSON);
                }
                db.collection('detections').insertMany(arrayJSON, (err, dbResult) => {
                    if (err) {
                        res.statusCode = 500;
                        res.json({ 'error' : 'Internal Server Error' });
                        throw err;
                    } else {
                        client.quit();
                        res.json({
                            'success' : 'Dump successfully'
                        });
                    }
                });
            }
        });

    }

    /**
     * @api {post} /vision/v1/detections/videos/search Search detections
     * @apiName postSearchVideosDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Object} detections Return all request detections (by colors and class)
     *
     * @apiParam {class} Array of suggested class
     * @apiParam {colors} Array of suggested colors
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postSearchVideosDetections(db, req, res, sequencer) {

        const redis = require('redis');
        const client = redis.createClient();
        const jsonpack = require('jsonpack');
        let saveJSON = {};
        let arrayJSON = [];

        /* Get areas MongoDB datas */
        db.collection('areas').find( { "assoc" : req.params.id } ).toArray((err, areas) => {

            if (err) {

                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;

            } else {

                let reqClasses;
                let reqColors;

                /* Filters options checkbox by classes and colors */
                if (typeof req.query.classes == 'string')
                    reqClasses = [parseInt(req.query.classes)];
                else if (req.query.classes != undefined)
                    reqClasses = req.query.classes.map(Number);
                else {
                    reqClasses = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
                }

                if (typeof req.query.colors == 'string')
                    reqColors = [parseInt(req.query.colors)];
                else if (req.query.colors != undefined)
                    reqColors = req.query.colors.map(Number);
                else
                    reqColors = [0, 1, 2, 3, 4, 5, 6, 7, 8];

                /* MongoDB request */
                let request = {
                    "assoc": req.params.id,
                    $and: [
                        {
                            "dets.class": {
                                $in: reqClasses
                            }
                        },
                        {
                            "dets.color": {
                                $in: reqColors
                            }
                        }
                    ]
                };

                /* Get MongoDB datas */
                db.collection('detections').find(request).toArray((err, detections) => {

                    if (err) {

                        res.statusCode = 500;
                        res.json({ 'error' : 'Internal Server Error' });
                        throw err;

                    } else {

                        /* Get Redis Datas */
                        client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                            if (replies) {
                                for (let i = 0; i < replies.length; i++) {
                                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                    if (saveJSON.assoc == req.params.id)
                                        arrayJSON.push(saveJSON);
                                }
                            }
                            client.quit();

                            /* Filters Redis datas */
                            let newArray = [];

                            for (var sequence in arrayJSON) {
                                var inside = false;

                                for (var detection in arrayJSON[sequence].dets) {
                                    if (inside == false && reqClasses.includes(arrayJSON[sequence].dets[detection].class)) {
                                        newArray.push(arrayJSON[sequence]);
                                        inside = true;
                                    }
                                }
                            }

                            /* Concat two Databases */
                            detections = detections.concat(newArray);

                            /* Verify areas */
                            let areasDetections = [];

                            if (req.query.areas && areas) {
                                for (var i = 0; i < detections.length; i++) {
                                    var passed = false;

                                    for (var det in detections[i].dets) {
                                        areas.forEach((area) => {
                                            if (passed == false
                                                && req.query.areas.includes(area.name)
                                                && reqClasses.includes(detections[i].dets[det].class)
                                                && ((detections[i].dets[det].x >= area.coords[0]
                                                    && detections[i].dets[det].x <= area.coords[0] + area.coords[2]
                                                    && detections[i].dets[det].y >= area.coords[1]
                                                    && detections[i].dets[det].y <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x + detections[i].dets[det].w >= area.coords[0]
                                                        && detections[i].dets[det].x + detections[i].dets[det].w <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y >= area.coords[1]
                                                        && detections[i].dets[det].y <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x >= area.coords[0]
                                                        && detections[i].dets[det].x <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h >= area.coords[1]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x + detections[i].dets[det].w >= area.coords[0]
                                                        && detections[i].dets[det].x + detections[i].dets[det].w <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h >= area.coords[1]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h <= area.coords[1] + area.coords[3]))) {

                                                areasDetections.push(detections[i]);
                                                passed = true;
                                            }
                                        });
                                    }
                                }
                                detections = areasDetections;
                                
                                res.json({detections});
                            } else {
                                res.json({detections});
                            }
                        });

                    }

                });
            }

        });

    }

}
