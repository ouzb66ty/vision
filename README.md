# Vision

An Orizon module, darknet visual recognition.

## API

on 127.0.0.1:8003
[GET] /detections obtain all detections dumped
[PUT] /detections/{idCam} active detection on IP Cam
[POST] /detections/{redisDump} dump all redis detections in MongoDB

## Websockets

on 127.0.0.1:8002
Socket.IO event is `dets` to get IRT detections, example :

	{
		"filename" : "../public/sequences/Plage 1/16992ad1789.jpg", 
		"res" : NumberInt(1), 
		"dets" : [{
			"xmin" : 0.8074376583099365, 
			"ymin" : 0.9372540712356567, 
			"xmax" : 0.3399723470211029, 
			"ymax" : 0.41154518723487854, 
			"class" : NumberInt(2)
		}],
		"join" : "5c8ff25fb494f046fd912fef"
	}

## Starting

    npm start

## Authors

orizon-corp
